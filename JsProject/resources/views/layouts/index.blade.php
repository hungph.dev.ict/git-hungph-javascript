<!DOCTYPE html>
<html>
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<h1>Javascript HungPH</h1>
	<div class="container">
		<div class="row">
            {{-- Form --}}
			<form class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-sm-2" for="id"> Company code </label>
					<div class="col-sm-10">
						<input type="id" class="form-control" name="">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="name"> Company name </label>
					<div class="col-sm-10">
						<input type="name" class="form-control" name="">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="submit" class="btn btn-info">Search</button>
					</div>
				</div>
			</form>

            {{-- Button --}}
			<div class="button">
				<button type="button" class="btn btn-success"> Add </button>
				<button type="button" class="btn btn-success"> Import </button>
				<span class="dropdown">
					<button type="button" class="btn" data-toggle="dropdown">Export
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#">export1</a></li>
						<li><a href="#">export2</a></li>
						<li><a href="#">export3</a></li>
					</ul>
				</span>
			</div>

            {{-- Table --}}
            <table class="table table-bordered">
                <tr>
                    <th>List all company using services</th>
                </tr>  
                <tr>
                    <td>
                        <table class="table table-bordered">
                            <thead>
                                <tr class="info">
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </td>
                </tr>
            </table>
		</div>
	</div>
</body>
</html>